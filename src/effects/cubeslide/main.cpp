#include "cubeslide.h"

namespace KWin
{

KWIN_EFFECT_FACTORY_SUPPORTED(CubeSlideEffectFactory,
                              CubeSlideEffect,
                              "metadata.json",
                              return CubeSlideEffect::supported();)

} // namespace KWin

#include "main.moc"
